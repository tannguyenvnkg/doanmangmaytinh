/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

/**
 *
 * @author tanng
 */
public class Student {
    private String studentID;
    private String studentName;
    private String mathScore;
    private String literatureScore;
    private String englishScore;

    public Student(String studentID, String studentName, String mathScore, String literatureScore, String englishScore) {
        this.studentID = studentID;
        this.studentName = studentName;
        this.mathScore = mathScore;
        this.literatureScore = literatureScore;
        this.englishScore = englishScore;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getMathScore() {
        return mathScore;
    }

    public void setMathScore(String mathScore) {
        this.mathScore = mathScore;
    }

    public String getLiteratureScore() {
        return literatureScore;
    }

    public void setLiteratureScore(String literatureScore) {
        this.literatureScore = literatureScore;
    }

    public String getEnglishScore() {
        return englishScore;
    }

    public void setEnglishScore(String englishScore) {
        this.englishScore = englishScore;
    }

    public String getAverageScore(){
        try {
            float mathScore = Float.parseFloat(getMathScore());
            float literatureScore = Float.parseFloat(getLiteratureScore());
            float englishScore = Float.parseFloat(getEnglishScore());
            
            return String.valueOf((mathScore+literatureScore+englishScore)/3);
        } catch (Exception e) {
            System.err.println("Parse Score Error: ");
            e.printStackTrace();
            return "";
        }
    }
    @Override
    public String toString() {
        return "Student{" + "studentID=" + studentID + ", studentName=" + studentName + ", mathScore=" + mathScore + ", literatureScore=" + literatureScore + ", englishScore=" + englishScore + '}';
    }
    
}
