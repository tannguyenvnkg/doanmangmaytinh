/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Random;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import static javax.crypto.Cipher.SECRET_KEY;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author sipha
 */
public class DES {
    private String SECRET_KEY;
    private String original;
    private static final String UNICODE_FORMAT = "UTF8"; 
    
    public String getSECRET_KEY() {
        return SECRET_KEY;
    }

    public void setSECRET_KEY(String SECRET_KEY) {
        this.SECRET_KEY = SECRET_KEY;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public DES() {
        this.SECRET_KEY = "";
        this.original = "";
    }

    public DES(String SECRET_KEY, String original) {
        this.SECRET_KEY = SECRET_KEY;
        this.original = original;
    }
    
    public String DESEncrypted (String SECRET_KEY, String original) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(SECRET_KEY.getBytes(), "DES");
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte [] plainText = original.getBytes(UNICODE_FORMAT);
            byte[] byteEncrypted = cipher.doFinal(plainText);
            String encrypted = Base64.getEncoder().encodeToString(byteEncrypted);
            return encrypted;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String DESDecrypted (String SECRET_KEY, String encrypted) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(SECRET_KEY.getBytes(), "DES");
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte [] encryptedtext = Base64.getDecoder().decode(encrypted);
            byte[] byteDecrypted = cipher.doFinal(encryptedtext);
            String decrypted = new String(byteDecrypted);
            return decrypted;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
    
}
