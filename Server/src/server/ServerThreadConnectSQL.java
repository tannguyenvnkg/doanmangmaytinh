/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author sipha
 */
public class ServerThreadConnectSQL implements Runnable{
    private Scanner in = null;
    private PrintWriter out = null;
    private Socket socket;
    private String name;
    
    private DBAccess conn;
    private String DES_KEY = "TEACUPTS";
    
    public ServerThreadConnectSQL(Socket socket, String name) throws IOException{
        this.socket = socket;
        this.name = name;
        this.in = new Scanner(this.socket.getInputStream());
        this.out = new PrintWriter(this.socket.getOutputStream(),true);
        new Thread(this).start();
    }
   
    public void run() {
        out.println(true); // return true when connect to server successful and start formConnectSQL
        
        //formConnectSQL
        connectToSQL();
        
        //insert student to database
        try {
            insertStudent();
        } catch(Exception e){
            System.err.println("Insert Student Error: ");
            e.printStackTrace();
        }
        
        //get list and send it to client
        try {
            // send list student to client
            sendListStudent();
        } catch (SQLException ex) {
            System.err.println("Send List Student Error: ");
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ServerThreadConnectSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(ServerThreadConnectSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(ServerThreadConnectSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(ServerThreadConnectSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(ServerThreadConnectSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ServerThreadConnectSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //close socket
        try {
            socket.close();
            System.out.println("Close Socket");
        } catch (IOException e) {
            System.err.println("Close Socket Error: ");
            e.printStackTrace();
        }
        
    }
    
    private void connectToSQL(){
        String str = in.nextLine().trim();
        Scanner sc = new Scanner(str);
        sc.useDelimiter("@");
        String usernameSQL = sc.next();
        String passwordSQL = sc.next();
        String portSQL = sc.next();
        
        conn = new DBAccess(usernameSQL,passwordSQL,portSQL); // create DBAccess when connect to SQL
        try {
            out.println(conn.isConnectSuccess());
        } catch (Exception e) {
            System.err.println("Connect To SQL Error: ");
            e.printStackTrace();
        }
    }
    
    private void insertStudent() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
        String str = in.nextLine();
        System.out.println(str);
        Scanner sc = new Scanner(str);
        sc.useDelimiter("@");
        String studentName = sc.next();
        String studentID = sc.next();
        String mathScore = sc.next();
        String literatureScore = sc.next();
        String EnglishScore = sc.next();
        
        DES des = new DES();
        studentName = des.DESEncrypted(DES_KEY, studentName);
        studentID = des.DESEncrypted(DES_KEY, studentID);
        mathScore = des.DESEncrypted(DES_KEY, mathScore);
        literatureScore = des.DESEncrypted(DES_KEY, literatureScore);
        EnglishScore = des.DESEncrypted(DES_KEY, EnglishScore);
        
        try {
            String query = "insert into SINHVIEN values('"+studentID+"','"+studentName+"','"+mathScore+"','"+literatureScore+"','"+EnglishScore+"')";
            boolean isInsertSuccessfully = conn.excuteQuery(query);
            out.println(isInsertSuccessfully); // return insert success or failure
            
        } catch (Exception e) {
            System.err.println("Insert Student Error: ");
            e.printStackTrace();
        }
        
    }
    
    private void sendListStudent() throws SQLException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
        try {
            boolean getlist = in.nextBoolean();
            if(getlist){
                ArrayList<Student> students = new ArrayList();
                addList(students);

                Gson gson = new Gson();
                out.println(students.size()); // pass size of arraylist student to client
                System.out.println("List Size: " + students.size());
                for (int i = 0; i < students.size(); i++) {
                    String json = gson.toJson(students.get(i)); 
                    json = json.replace(' ', '@'); // out.println gặp khoảng trẳng auto xuống dòng nên phải đổi khoảng trắng thành 1 ký tự
//                    System.out.println("Students["+i+"]: " + students.get(i).toString());
//                    System.out.println("Json Server : " + json);
                    out.println(json);
                }
            }
        } catch (Exception e) {
            System.err.println("Send List Student Error: ");
            e.printStackTrace();
        }
        
    }
    
    private void addList(ArrayList<Student> students) throws SQLException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
        String query = "select * from sinhvien";
        ResultSet rs = conn.getData(query);
        while (rs.next()) {  
            String studentID = rs.getString(1);
            String studentName = rs.getString(2);
            String mathScore = rs.getString(3);
            String literatureScore = rs.getString(4);
            String englishScore = rs.getString(5);
            
            DES des = new DES();
            studentName = des.DESDecrypted(DES_KEY, studentName);
            studentID = des.DESDecrypted(DES_KEY, studentID);
            mathScore = des.DESDecrypted(DES_KEY, String.valueOf(mathScore));
            literatureScore = des.DESDecrypted(DES_KEY, String.valueOf(literatureScore));
            englishScore = des.DESDecrypted(DES_KEY, String.valueOf(englishScore));
            
            students.add(new Student(studentID, studentName, mathScore, literatureScore, englishScore));
        }
    }
            
}
