/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author tanng
 */
public class DBAccess {
    private Connection con;
    private Statement stmt;
    private boolean connectSuccess; // return this value to client
    
    private String usernameSQL;
    private String passwordSQL;
    private String portSQL;

    public DBAccess(String usernameSQL, String passwordSQL, String portSQL){
        this.usernameSQL = usernameSQL;
        this.passwordSQL = passwordSQL;
        this.portSQL = portSQL;
        try {
            MyConnection myConnection = new MyConnection(usernameSQL,passwordSQL,portSQL);
            con = myConnection.getConnection();
            stmt = con.createStatement();
            connectSuccess = true;
        } catch (Exception e) {
            System.err.println("DBAccess: " + e.toString());
            connectSuccess = false;
        }
    }
    
    public boolean isConnectSuccess() {
        return connectSuccess;
    }
    
    public boolean excuteQuery(String query){
        try {
            stmt.execute(query);
            return true;
        } catch (Exception e) {
            System.out.println("Không thể excute query");
            System.err.println(e.toString());
            return false;
        }
    }
    
    public ResultSet getData(String query){
        try {
            ResultSet rs = stmt.executeQuery(query);
            return rs;
        } catch (Exception e) {
            System.err.println(e.toString());
            return null;
        }
    }
}
