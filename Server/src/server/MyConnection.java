/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author sipha
 */
public class MyConnection {
    String usernameSQL;
    String passwordSQL;
    String portSQL;

    public MyConnection(String usernameSQL, String passwordSQL, String portSQL) {
        this.usernameSQL = usernameSQL;
        this.passwordSQL = passwordSQL;
        this.portSQL = portSQL;
    }
    
    public Connection getConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:"+ portSQL +";databaseName=QuanLySinhVien;user="+ usernameSQL +";password=" + passwordSQL;
            Connection con = DriverManager.getConnection(url);
            System.out.println("Kết nối cơ sở dữ liệu thành công");
            return con;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(),"loi",JOptionPane.ERROR_MESSAGE);
            return null;
        } 
    }
}
