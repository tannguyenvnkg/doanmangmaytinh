/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;
import java.io.*;
import java.net.*;
import java.util.*;
/**
 *
 * @author sipha
 */
public class TCPServer {
    static final int PORT = 1234;
    private ServerSocket server = null;

    public TCPServer() {
        try {
            server = new ServerSocket(PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public  void connectServer() { // connect server from Client
        Socket socket = null;
        int i = 0;
        System.out.println("Server đang lắng nghe .....");
        try {
            while ((socket = server.accept()) != null) {                
                new  ServerThreadConnectSQL(socket, "Client: " + i++);
                System.out.println("Đã kết nối với Client" + i++ + "Với cơ sở dữ liệu");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public static void main(String[] args) {
        new TCPServer().connectServer();
    }
    
}
